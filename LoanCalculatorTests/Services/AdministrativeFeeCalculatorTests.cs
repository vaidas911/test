﻿using LoanCalculator.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LoanCalculator.Tests.Services
{
	[TestClass]
	public class AdministrativeFeeCalculatorTests
	{
		[TestMethod]
		public void PercentageIsUsedAsFeeIfPercentageLessThanAmount()
		{
			var loanAmount = 500000;
			var administrativeFeeAmount = 10000;
			var administrativeFeePercentage = 1;

			var calculator = CreateCalculator();

			var result = calculator.Calculate(
				loanAmount,
				administrativeFeePercentage,
				administrativeFeeAmount
			);

			Assert.AreEqual(5000, result);
		}

		[TestMethod]
		public void AmountIsUsedAsFeeIfPercentageGreaterThanAmount()
		{
			var loanAmount = 500000;
			var administrativeFeeAmount = 10000;
			var administrativeFeePercentage = 10;

			var calculator = CreateCalculator();

			var result = calculator.Calculate(
				loanAmount,
				administrativeFeePercentage,
				administrativeFeeAmount
			);

			Assert.AreEqual(10000, result);
		}

		private AdministrativeFeeCalculator CreateCalculator()
		{
			return new AdministrativeFeeCalculator();
		}
	}
}

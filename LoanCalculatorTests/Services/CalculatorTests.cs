﻿using LoanCalculator.Models;
using LoanCalculator.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LoanCalculator.Tests.Services
{
	[TestClass]
	public class CalculatorTests
	{
		private Mock<IMonthlyCostCalculator> monthlyConstCalculatorMock;
		private Mock<IInterestRateCalculator> interestRateCalculatorMock;
		private Mock<IAdministrativeFeeCalculator> administrativeFeeCalculatorMock;

		private Parameters parameters;

		private double monthlyAmount = 5000;
		private double interestRate = 100000;
		private double administrativeFee = 60000;

		[TestInitialize]
		public void TestInitialize()
		{
			parameters = new Parameters();

			monthlyConstCalculatorMock = new Mock<IMonthlyCostCalculator>();
			monthlyConstCalculatorMock
				.Setup(
					x => x.Calculate(
						It.IsAny<double>(),
						It.IsAny<double>(),
						It.IsAny<double>()
					)
				)
				.Returns(() => monthlyAmount);
			
			interestRateCalculatorMock = new Mock<IInterestRateCalculator>();
			interestRateCalculatorMock.Setup(
					x => x.Calculate(
						It.IsAny<double>(),
						It.IsAny<double>(),
						It.IsAny<double>()
					)
				)
				.Returns(() => interestRate);

			administrativeFeeCalculatorMock = new Mock<IAdministrativeFeeCalculator>();

			administrativeFeeCalculatorMock.Setup(
					x => x.Calculate(
						It.IsAny<double>(),
						It.IsAny<double>(),
						It.IsAny<double>()
					)
				)
				.Returns(() => administrativeFee);
		}

		[TestMethod]
		public void AdministrativeFeeCalculatorCalledOnce()
		{
			parameters = new Parameters
			{
				LoanAmount = 500000,
				AdministrativeFeeAmount = 10000,
				AdministrativeFeePercentage = 10,
				AnnualInterestRate = 5,
				DurationInYears = 10
			};

			var calculator = CreateCalculator();

			calculator.CalculateLoanResult(parameters);

			administrativeFeeCalculatorMock.Verify(
				x => x.Calculate(
					It.IsAny<double>(),
					It.IsAny<double>(),
					It.IsAny<double>()
				),
				Times.Once
			);

			administrativeFeeCalculatorMock.Verify(
				x => x.Calculate(
					parameters.LoanAmount,
					parameters.AdministrativeFeePercentage,
					parameters.AdministrativeFeeAmount
				),
				Times.Once
			);
		}

		[TestMethod]
		public void MonthlyCostCalculatorCalledOnce()
		{
			parameters = new Parameters
			{
				LoanAmount = 500000,
				AdministrativeFeeAmount = 10000,
				AdministrativeFeePercentage = 10,
				AnnualInterestRate = 5,
				DurationInYears = 10
			};

			var calculator = CreateCalculator();

			calculator.CalculateLoanResult(parameters);

			monthlyConstCalculatorMock.Verify(
				x => x.Calculate(
					It.IsAny<double>(),
					It.IsAny<double>(),
					It.IsAny<double>()
				),
				Times.Once
			);

			monthlyConstCalculatorMock.Verify(
				x => x.Calculate(
					parameters.LoanAmount,
					parameters.AnnualInterestRate,
					parameters.DurationInYears
				),
				Times.Once
			);
		}

		[TestMethod]
		public void InterestRateCalculatorMockCalledOnce()
		{
			parameters = new Parameters
			{
				LoanAmount = 500000,
				AnnualInterestRate = 5,
				DurationInYears = 10
			};

			var calculator = CreateCalculator();

			calculator.CalculateLoanResult(parameters);

			interestRateCalculatorMock.Verify(
				x => x.Calculate(
					It.IsAny<double>(),
					It.IsAny<double>(),
					It.IsAny<double>()
				),
				Times.Once
			);

			interestRateCalculatorMock.Verify(
				x => x.Calculate(
					parameters.LoanAmount,
					parameters.DurationInYears,
					monthlyAmount
				),
				Times.Once
			);
		}

		[DataTestMethod]
		[DataRow(500000, 10, 100000, 10000, 2.2)]
		[DataRow(500000, 1, 50000, 6000, 11.2)]
		[DataRow(100000, 1, 1200, 1200, 2.4)]
		public void ArpTest(
			double loanAmount,
			int duration,
			double totalInterestRate,
			double totalAdministrativeFee,
			double expectedValue)
		{
			interestRate = totalInterestRate;
			administrativeFee = totalAdministrativeFee;
			
			parameters = new Parameters
			{
				LoanAmount = loanAmount,
				DurationInYears = duration
			};

			var calculator = CreateCalculator();

			var result = calculator.CalculateLoanResult(parameters);

			Assert.AreEqual(expectedValue, result.ArpRate);
		}

		private Calculator CreateCalculator()
		{
			return new Calculator(
				monthlyConstCalculatorMock.Object,
				interestRateCalculatorMock.Object,
				administrativeFeeCalculatorMock.Object
			);
		}
	}
}

﻿using System;
using FluentAssertions;
using LoanCalculator.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LoanCalculator.Tests.Services
{
	[TestClass]
	public class MonthlyCostCalculatorTests
	{
		[TestMethod]
		public void ThrowsInvalidArgumentExceptionIfYearIsZero()
		{
			var calculator = CreateCalculator();

			Action act = () => calculator.Calculate(
				1000,
				5,
				0
			);

			act.Should()
				.Throw<ArgumentException>()
				.WithMessage("Duration must be positive number.");
			
		}

		[DataTestMethod]
		[DataRow(144, 0, 1, 12)]
		[DataRow(1200, 10, 1, 105.5)]
		[DataRow(500000, 5, 10, 5303.28)]
		public void ReturnsMonthlyCostByFormula(
			double loanAmount,
			double annualInterestRate,
			double durationInYears,
			double expectedResult)
		{
			var calculator = CreateCalculator();

			var result = calculator.Calculate(
				loanAmount,
				annualInterestRate,
				durationInYears
			);

			Assert.AreEqual(expectedResult, Math.Round(result, 2));
		}

		private MonthlyCostCalculator CreateCalculator()
		{
			return new MonthlyCostCalculator();
		}
	}
}

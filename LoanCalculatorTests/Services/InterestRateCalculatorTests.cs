﻿using System;
using FluentAssertions;
using LoanCalculator.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LoanCalculator.Tests.Services
{
	[TestClass]
	public class InterestRateCalculatorTests
	{
		[TestMethod]
		public void ThrowsInvalidArgumentExceptionIfYearIsZero()
		{
			var calculator = CreateCalculator();

			Action act = () => calculator.Calculate(
				1000,
				0,
				2
			);

			act.Should()
				.Throw<ArgumentException>()
				.WithMessage("Duration must be positive number.");
			
		}
		
		[DataTestMethod]
		[DataRow(500000, 10, 5000, 100000)]
		[DataRow(12000, 10, 100, 0)]
		public void TotalInterestRateIsCalculatedFromTotalLoanAndMonthlyRate(
			double loanAmount,
			double durationInYears,
			double monthlyCost,
			double expectedResult)
		{
			var calculator = CreateCalculator();

			var result = calculator.Calculate(loanAmount, durationInYears, monthlyCost);

			Assert.AreEqual(expectedResult, result);
		}

		private InterestRateCalculator CreateCalculator()
		{
			return new InterestRateCalculator();
		}
	}
}

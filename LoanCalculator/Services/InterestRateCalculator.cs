﻿using System;

namespace LoanCalculator.Services
{
	public class InterestRateCalculator : IInterestRateCalculator
	{
		public double Calculate(
			double loanAmount,
			double durationInYears,
			double monthlyCost)
		{
			if (durationInYears <= 0)
			{
				throw new ArgumentException("Duration must be positive number.");
			}

			var numberOfMonths = durationInYears * 12;

			var totalAmountPaid = numberOfMonths * monthlyCost;

			return totalAmountPaid - loanAmount;
		}
	}
}
﻿using System;

namespace LoanCalculator.Services
{
	public class MonthlyCostCalculator: IMonthlyCostCalculator
	{
		public double Calculate(
			double loanAmount,
			double annualInterestRate,
			double durationInYears)
		{
			if (durationInYears <= 0)
			{
				throw new ArgumentException("Duration must be positive number.");
			}

			var numberOfMonths = durationInYears * 12;
			var monthlyInterestRate = annualInterestRate / 12D / 100D;

			if (monthlyInterestRate == 0)
			{
				return loanAmount / numberOfMonths;
			}

			var monthlyRate =
				loanAmount * monthlyInterestRate *
				Math.Pow(1 + monthlyInterestRate, numberOfMonths) /
				(Math.Pow(1 + monthlyInterestRate, numberOfMonths) - 1);

			return monthlyRate;
		}
	}
}

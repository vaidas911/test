﻿using System;
using LoanCalculator.Models;

namespace LoanCalculator.Services
{
	public class Calculator : ICalculator
	{
		private readonly IMonthlyCostCalculator monthlyCostCalculator;
		private readonly IInterestRateCalculator interestRateCalculator;
		private readonly IAdministrativeFeeCalculator administrativeFeeCalculator;

		public Calculator(
			IMonthlyCostCalculator monthlyCostCalculator,
			IInterestRateCalculator interestRateCalculator,
			IAdministrativeFeeCalculator administrativeFeeCalculator)
		{
			this.monthlyCostCalculator = monthlyCostCalculator;
			this.interestRateCalculator = interestRateCalculator;
			this.administrativeFeeCalculator = administrativeFeeCalculator;
		}

		public CalculationResult CalculateLoanResult(Parameters parameters)
		{
			var monthlyCost = GetMonthlyCost(parameters);
			var interestRate = GetTotalInterestRate(parameters, monthlyCost);
			var administrativeFee = GetTotalAdministrativeFee(parameters);

			var arp = GetArpRate(
				parameters,
				interestRate,
				administrativeFee
			);

			return new CalculationResult(
				Math.Round(arp, 2),
				Math.Round(monthlyCost, 2),
				Math.Round(interestRate, 2),
				administrativeFee
			);
		}

		private double GetArpRate(
			Parameters parameters,
			double interestRate,
			double administrativeFee)
		{
			return (interestRate + administrativeFee)
				/ parameters.LoanAmount
				/ parameters.DurationInYears * 100;
		}

		private double GetMonthlyCost(Parameters parameters)
		{
			return monthlyCostCalculator.Calculate(
				parameters.LoanAmount,
				parameters.AnnualInterestRate,
				parameters.DurationInYears
			);
		}

		private double GetTotalInterestRate(Parameters parameters, double monthlyCost)
		{
			return interestRateCalculator.Calculate(
				parameters.LoanAmount,
				parameters.DurationInYears,
				monthlyCost
			);
		}

		private double GetTotalAdministrativeFee(Parameters parameters)
		{
			return administrativeFeeCalculator.Calculate(
				parameters.LoanAmount,
				parameters.AdministrativeFeePercentage,
				parameters.AdministrativeFeeAmount
			);
		}
	}
}

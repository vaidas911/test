﻿namespace LoanCalculator.Services
{
	public class AdministrativeFeeCalculator : IAdministrativeFeeCalculator
	{
		public double Calculate(
			double loanAmount,
			double administrativeFeePercentage,
			double administrativeFeeAmount)
		{
			var percentageFee = loanAmount * administrativeFeePercentage / 100;

			return percentageFee < administrativeFeeAmount
				? percentageFee
				: administrativeFeeAmount;
		}
	}
}
﻿using LoanCalculator.Models;

namespace LoanCalculator.Services
{
	public interface ICalculator
	{
		CalculationResult CalculateLoanResult(Parameters parameters);
	}
}
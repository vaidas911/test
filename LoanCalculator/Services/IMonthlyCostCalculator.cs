﻿namespace LoanCalculator.Services
{
	public interface IMonthlyCostCalculator
	{
		double Calculate(
			double loanAmount,
			double annualInterestRate,
			double durationInYears);
	}
}
﻿namespace LoanCalculator.Services
{
	public interface IInterestRateCalculator
	{
		double Calculate(
			double loanAmount,
			double durationInYears,
			double monthlyCost);
	}
}

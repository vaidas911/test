﻿using FluentValidation;
using LoanCalculator.Models;

namespace LoanCalculator.Services
{
	public class ParametersValidator: AbstractValidator<Parameters>
	{
		public ParametersValidator()
		{
			RuleFor(x => x.DurationInYears).GreaterThan(0);
			RuleFor(x => x.LoanAmount).GreaterThan(0);
			RuleFor(x => x.AnnualInterestRate).GreaterThanOrEqualTo(0);
			RuleFor(x => x.AdministrativeFeeAmount).GreaterThanOrEqualTo(0);
			RuleFor(x => x.AdministrativeFeePercentage).GreaterThanOrEqualTo(0);
		}
	}
}

﻿using LoanCalculator.Models;

namespace LoanCalculator.Services
{
	public interface IParametersProvider
	{
		bool IsValid();
		Parameters Get();
		void Set(string[] args);
	}
}

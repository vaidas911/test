﻿using System;
using LoanCalculator.Models;
using MatthiWare.CommandLine;
using MatthiWare.CommandLine.Abstractions.Parsing;
using MatthiWare.CommandLine.Extensions.FluentValidations;
using Microsoft.Extensions.Options;

namespace LoanCalculator.Services
{
	public class ParametersProvider: IParametersProvider
	{
		private string[] args;
		private IParserResult<Parameters> parsedOptions;
		private readonly CommandLineParser<Parameters> parser;

		public ParametersProvider(IOptions<Parameters> options)
		{
			parser = SetupParser(options.Value);
			parser.UseFluentValidations(configurator => configurator.AddValidator<Parameters, ParametersValidator>());
		}
		
		public bool IsValid()
		{
			parsedOptions = parser.Parse(args);
			return !parsedOptions.HasErrors;
		}

		public Parameters Get()
		{
			if (parsedOptions == null)
			{
				throw new InvalidOperationException("IsValid method must be called first.");
			}

			return parsedOptions.Result;
		}

		public void Set(string[] value)
		{
			args = value;
		}

		private static CommandLineParser<Parameters> SetupParser(Parameters defaultParameters)
		{
			var parser = new CommandLineParser<Parameters>();
			parser
				.Configure(options => options.LoanAmount)
				.Name("a", "amount")
				.Description("The loan amount")
				.Required();

			parser
				.Configure(options => options.DurationInYears)
				.Name("d", "duration")
				.Description("The loan duration in years")
				.Required();

			parser
				.Configure(options => options.AdministrativeFeeAmount)
				.Name("f", "fee")
				.Description($"The loan administrative fee amount (default {defaultParameters.AdministrativeFeeAmount})")
				.Default(defaultParameters.AdministrativeFeeAmount)
				.Required(false);

			parser
				.Configure(options => options.AdministrativeFeePercentage)
				.Name("p", "percentage")
				.Description($"The loan administrative fee percentage (default {defaultParameters.AdministrativeFeePercentage})")
				.Default(defaultParameters.AdministrativeFeePercentage)
				.Required(false);

			parser
				.Configure(options => options.AnnualInterestRate)
				.Name("i", "interest")
				.Description($"Annual interest rate (default {defaultParameters.AnnualInterestRate})")
				.Default(defaultParameters.AnnualInterestRate)
				.Required(false);

			return parser;
		}

	}
}
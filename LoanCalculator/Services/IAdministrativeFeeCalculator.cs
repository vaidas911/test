﻿namespace LoanCalculator.Services
{
	public interface IAdministrativeFeeCalculator
	{
		double Calculate(
			double loanAmount,
			double administrativeFeePercentage,
			double administrativeFeeAmount);
	}
}
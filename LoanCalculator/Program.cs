﻿using System;
using LoanCalculator.Models;
using LoanCalculator.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LoanCalculator
{
	class Program
	{
		static void Main(string[] args)
		{
			var builder = new ConfigurationBuilder()
				.AddJsonFile(
					"appsettings.json",
					true,
					true
				)
				.AddEnvironmentVariables();

			var configuration = builder.Build();

			var services = new ServiceCollection();

			var serviceProvider = services
				.Configure<Parameters>(options => configuration.GetSection("Options").Bind(options))
				.AddSingleton<IMonthlyCostCalculator, MonthlyCostCalculator>()
				.AddSingleton<ICalculator, Calculator>()
				.AddSingleton<IMonthlyCostCalculator, MonthlyCostCalculator>()
				.AddSingleton<IInterestRateCalculator, InterestRateCalculator>()
				.AddSingleton<IAdministrativeFeeCalculator, AdministrativeFeeCalculator>()
				.AddSingleton<IParametersProvider, ParametersProvider>()
				.BuildServiceProvider();

			var parametersProvider = serviceProvider.GetRequiredService<IParametersProvider>();

			parametersProvider.Set(args);
			var calculator = serviceProvider.GetRequiredService<ICalculator>();
			while (true)
			{
				if (parametersProvider.IsValid())
				{
					Console.WriteLine("Loan calculation for parameters:");
					var parameters = parametersProvider.Get();
					Console.WriteLine(parameters);
					var result = calculator.CalculateLoanResult(parameters);
					Console.WriteLine();
					Console.WriteLine("Returned following result:");
					Console.WriteLine(result);
					Console.WriteLine("Do you want to do another calculation? Y/N (Default N)");
					var line = Console.ReadLine();
					if (line?.ToLower() == "y")
					{
						parametersProvider.Set(Array.Empty<string>());
						continue;
					}
					break;
				}
				else
				{
					Console.WriteLine("Please enter parameters or type 'exit' for exit.");
					var line = Console.ReadLine();
					if (line == "exit")
					{
						break;
					}
					parametersProvider.Set(line?.Split(" "));
					
				}
			}
		}
	}
}


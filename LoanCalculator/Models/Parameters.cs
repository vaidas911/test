﻿using System.Text;

namespace LoanCalculator.Models
{
	public class Parameters
	{
		public double LoanAmount { get; set; }
		public int DurationInYears { get; set; }

		//optional - defaults read from settings
		public double AnnualInterestRate { get; set; }
		public double AdministrativeFeePercentage { get; set; }
		public double AdministrativeFeeAmount { get; set; }

		public override string ToString()
		{
			var sb = new StringBuilder();
			sb.AppendLine($"Loan amount: {LoanAmount}");
			sb.AppendLine($"Loan duration in years: {DurationInYears}");
			sb.AppendLine($"Annual interest rate: {AnnualInterestRate}%");
			sb.AppendLine($"Administrative fee percentage: {AdministrativeFeePercentage}%");
			sb.AppendLine($"Administrative fee amount: {AdministrativeFeeAmount}");
			return sb.ToString();
		}
	}
}

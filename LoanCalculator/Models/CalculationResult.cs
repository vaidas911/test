﻿using System.Text;

namespace LoanCalculator.Models
{
	public class CalculationResult
	{
		public double ArpRate { get; }
		public double MonthlyCost { get; }
		public double TotalInterestRate { get; }
		public double TotalAdministrativeFee { get; }

		public CalculationResult(
			double arpRate,
			double monthlyCost,
			double totalInterestRate,
			double totalAdministrativeFee)
		{
			ArpRate = arpRate;
			MonthlyCost = monthlyCost;
			TotalInterestRate = totalInterestRate;
			TotalAdministrativeFee = totalAdministrativeFee;
		}

		public override string ToString()
		{
			var sb = new StringBuilder();
			sb.AppendLine($"Monthly cost: {MonthlyCost}");
			sb.AppendLine($"Total interest rate: {TotalInterestRate}");
			sb.AppendLine($"Total administrative fee: {TotalAdministrativeFee}");
			sb.AppendLine($"ARP rate: {ArpRate}%");
			return sb.ToString();
		}
	}
}
